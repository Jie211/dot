#include <iostream>
#include <vector>
#include <random>
#include <gsl/gsl_rng.h>
#include <omp.h>

int main() {
    long int N = 100000000;
//    long int N = 10;
    double sum = 0;
    std::vector<double> a(N, 0.0), b(N, 0.0);

    std::random_device rnd;
    std::mt19937 mt(rnd());
    std::uniform_real_distribution<> rand1(0, 1);

    gsl_rng * gslmt = gsl_rng_alloc (gsl_rng_mt19937);

    auto start = std::chrono::system_clock::now();
    for (int i = 0; i < N; ++i) {
//        a[i] = rand1(mt);
//        b[i] = rand1(mt);
        a[i] = gsl_rng_uniform_pos(gslmt);
        b[i] = gsl_rng_uniform_pos(gslmt);
    }
    auto end = std::chrono::system_clock::now();
    auto dur = end - start;
    auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
    std::cout << "prepare timer = " << msec << " msec" << std::endl;

    start = std::chrono::system_clock::now();
    for (int i = 0; i < N; ++i) {
        sum += a[i] + b[i];
    }
    end = std::chrono::system_clock::now();
    dur = end - start;
    msec = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
    std::cout << "timer = " << msec << " msec" << std::endl;
    return 0;
}